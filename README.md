Uses:
    - Spring Framework
    - Spring Boot
    - Spring Data JPA
    - Spring Security
    - OMDB Java API: https://github.com/Omertron/api-omdb
    - AngularJS