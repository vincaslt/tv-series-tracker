package tvseriestracker.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

import tvseriestracker.domain.User;

public interface UserRepository extends CrudRepository<User, UUID> {
	User findByUsernameAllIgnoreCase(String username);
}
