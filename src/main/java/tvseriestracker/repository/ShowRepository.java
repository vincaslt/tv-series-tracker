package tvseriestracker.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

import tvseriestracker.domain.Show;

public interface ShowRepository extends CrudRepository<Show, UUID> {
	Show findByImdbId(String imdbId);
}
