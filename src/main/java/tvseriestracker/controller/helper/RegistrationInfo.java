package tvseriestracker.controller.helper;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Size;

@Component
@PasswordMatches
public class RegistrationInfo {
	@NotBlank(message = "Username must not be blank!")
	@Length(min = 5, max = 15, message = "Username must be 5-15 characters long!")
	private String username;

	@ValidEmail
	@NotBlank(message = "Email must not be blank!")
	private String email;

	@Size(min = 6, max = 50, message = "Password must be at least 6 characters long!")
	private String password;

	private String passwordConfirmation;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}
}
