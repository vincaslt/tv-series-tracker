package tvseriestracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import tvseriestracker.dto.ShowDTO;
import tvseriestracker.dto.UserDTO;
import tvseriestracker.service.ShowService;

@Controller
public class ProfileController extends AbstractController {

	@Autowired
	private ShowService showService;

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public final String dashboardGET(Model model) {
		final UserDTO userDTO = currentAuthenticatedUser();
		List<ShowDTO> shows = showService.findUserShows(userDTO);
		if (shows.size() > 0)
			model.addAttribute("shows", shows);

		return DASHBOARD_TEMPLATE;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public final String indexGET() {
		final UserDTO userDTO = currentAuthenticatedUser();
		if (userDTO != null)
			return redirect(DASHBOARD_TEMPLATE);
		return redirect(LOGIN_TEMPLATE);
	}
}
