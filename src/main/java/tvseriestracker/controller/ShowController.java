package tvseriestracker.controller;

import com.omertron.omdbapi.OMDBException;
import com.omertron.omdbapi.OmdbApi;
import com.omertron.omdbapi.emumerations.ResultType;
import com.omertron.omdbapi.model.OmdbVideoBasic;
import com.omertron.omdbapi.model.OmdbVideoFull;
import com.omertron.omdbapi.model.SearchResults;
import com.omertron.omdbapi.tools.OmdbBuilder;

import org.jboss.logging.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import tvseriestracker.dto.ShowDTO;
import tvseriestracker.dto.UserDTO;
import tvseriestracker.service.ShowService;

@RestController
public class ShowController extends AbstractController {

	@Autowired
	private ShowService showService;

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public final ModelAndView searchPageGET(Model model) {
		model.addAttribute("user", currentAuthenticatedUser());

		return new ModelAndView(SEARCH_TEMPLATE, "model", model);
	}

	@RequestMapping(value = "/show/search", method = RequestMethod.POST)
	public @ResponseBody
	MappingJackson2JsonView searchShowPOST(@Param String title) {
		MappingJackson2JsonView json = new MappingJackson2JsonView();
		if (title != null && title.length() > 0) {
			OmdbApi api = new OmdbApi();
			try {
				SearchResults results = api.search(new OmdbBuilder().setSearchTerm(title).setTypeSeries().build());
				List<OmdbVideoBasic> info = results.getResults();
				if (!results.isResponse())
					json.addStaticAttribute("Error", "No show with this title has been found, please check the title for mistakes, and try again");
				else {
					List<OmdbVideoFull> showList = new ArrayList<>();
					for (OmdbVideoBasic i : info) {
						OmdbVideoFull fullInfo = api.getInfo(new OmdbBuilder().setImdbId(i.getImdbID()).build());
						if (isShowValid(fullInfo) && !showList.contains(fullInfo)) {
							showList.add(fullInfo);
						}
					}
					json.addStaticAttribute("shows", showList);
					UserDTO user = currentAuthenticatedUser();
					List<ShowDTO> addedShows = showService.findUserShows(user);
					List<String> added = addedShows.stream().map(ShowDTO::getImdbId).collect(Collectors.toList());
					json.addStaticAttribute("added", added);
				}
			}
			catch(OMDBException e) {
				e.printStackTrace();
			}
		}
		return json;
	}

	@RequestMapping(value = "/show/add", method = RequestMethod.POST)
	public final boolean addShowPOST(@Param String imdbId) {
		OmdbApi api = new OmdbApi();
		try {
			OmdbVideoFull info = api.getInfo(new OmdbBuilder().setImdbId(imdbId).build());
			if (info != null && info.isResponse() && isShowValid(info)) {
				UserDTO user = currentAuthenticatedUser();
				ShowDTO showDTO = new ShowDTO(UUID.randomUUID(), info.getImdbID(),
						info.getTitle(), info.getPoster(), info.getImdbRating(), info.getPlot(), info.getYear());
				return showService.addShow(showDTO, user);
			}
		} catch (OMDBException e) {
			e.printStackTrace();
		}
		return false;
	}

	@RequestMapping(value = "/show/remove", method = RequestMethod.POST)
	public final boolean removeShowPOST(@Param String imdbId) {
		UserDTO userDTO = currentAuthenticatedUser();
		ShowDTO showDTO = showService.findByImdbId(imdbId);
		return showService.removeShow(showDTO, userDTO);
	}

	private boolean isShowValid(OmdbVideoFull info) {
		return (!info.getWriter().equals("N/A") || !info.getPoster().equals("N/A")) &&
				info.getType().equalsIgnoreCase(ResultType.SERIES.toString());
	}
}
