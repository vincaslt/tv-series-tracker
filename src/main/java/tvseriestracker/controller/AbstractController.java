package tvseriestracker.controller;

import com.sun.istack.internal.Nullable;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import tvseriestracker.config.SecurityConfig;
import tvseriestracker.dto.UserDTO;

public abstract class AbstractController {

	public static final String INDEX_TEMPLATE = "index";
	public static final String LOGIN_TEMPLATE = "login";
	public static final String DASHBOARD_TEMPLATE = "dashboard";
	public static final String SEARCH_TEMPLATE = "search";
	public static final String REGISTRATION_TEMPLATE = "register";

	@Nullable
	final UserDTO currentAuthenticatedUser() {
		UserDTO userDTO = null;
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.getPrincipal() instanceof SecurityConfig.SpringUserDetails) {
			final SecurityConfig.SpringUserDetails userDetails = (SecurityConfig.SpringUserDetails) authentication.getPrincipal();
			userDTO = userDetails.getUserDTO();
		}
		return userDTO;
	}

	protected String redirect(String template) {
		return "redirect:/" + template;
	}
}
