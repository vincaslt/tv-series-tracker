package tvseriestracker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

import tvseriestracker.controller.helper.RegistrationInfo;
import tvseriestracker.service.UserService;

@Controller
public class RegistrationController extends AbstractController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public final String registerGET(Model model) {
		model.addAttribute("registrationInfo", new RegistrationInfo());

		return REGISTRATION_TEMPLATE;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public final String registerPOST(@Valid RegistrationInfo registrationInfo,
									 BindingResult bindingResult, Model model,
									 RedirectAttributes redirectAttributes) {
		if (!bindingResult.hasErrors()) {
			UserService.RegistrationStatus status = userService.createUser(registrationInfo);
			if (status == UserService.RegistrationStatus.SUCCESS) {
				redirectAttributes.addFlashAttribute("alertSuccess", "Account created successfully!");
				return redirect(LOGIN_TEMPLATE);
			} else if (status == UserService.RegistrationStatus.EXISTS)
				model.addAttribute("alertError", "Username already exists!");
			else model.addAttribute("alertError", "Registration failed, please try again!");
		}

		model.addAttribute("registrationInfo", registrationInfo);
		return REGISTRATION_TEMPLATE;
	}
}
