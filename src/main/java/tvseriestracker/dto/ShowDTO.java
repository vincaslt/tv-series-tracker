package tvseriestracker.dto;

import java.util.UUID;

public class ShowDTO {
	// TODO add users tracking field
	private UUID id;
	private String imdbId;
	private String title;
	private String posterURL;
	private String rating;
	private String description;
	private String year;

	public ShowDTO() {
	}

	public ShowDTO(UUID id, String imdbId, String title,
				   String posterURL, String rating, String description, String year) {
		this.id = id;
		this.imdbId = imdbId;
		this.title = title;
		this.posterURL = posterURL;
		this.rating = rating;
		this.description = description;
		this.year = year;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPosterURL() {
		return posterURL;
	}

	public void setPosterURL(String posterURL) {
		this.posterURL = posterURL;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return String.format("ShowDTO [imdbId: %s; title: %s; rating: %s; description: %s; poster: %s]",
				this.getImdbId(), this.getTitle(), this.getRating(), this.getDescription(), this.getPosterURL());
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public boolean equals(Object obj) {
		return getImdbId().equals(((ShowDTO)obj).getImdbId());
	}
}
