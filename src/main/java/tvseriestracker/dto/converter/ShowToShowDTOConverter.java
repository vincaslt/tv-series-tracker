package tvseriestracker.dto.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import tvseriestracker.domain.Show;
import tvseriestracker.dto.ShowDTO;

@Component
public class ShowToShowDTOConverter implements Converter<Show, ShowDTO> {
	@Override
	public ShowDTO convert(Show show) {
		ShowDTO showDTO = null;
		if (show != null) {
			showDTO = new ShowDTO(show.getId(), show.getImdbId(),
				show.getTitle(), show.getPosterURL(), show.getRating(), show.getDescription(), show.getYear());
		}
		return showDTO;
	}
}
