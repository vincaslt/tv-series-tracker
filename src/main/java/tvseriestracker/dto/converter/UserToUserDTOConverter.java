package tvseriestracker.dto.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import tvseriestracker.domain.User;
import tvseriestracker.dto.UserDTO;

@Component
public final class UserToUserDTOConverter implements Converter<User, UserDTO> {

	private ShowToShowDTOConverter showConverter;

	@Autowired
	public UserToUserDTOConverter(ShowToShowDTOConverter showConverter) {
		this.showConverter = showConverter;
	}

	@Override
	public UserDTO convert(User user) {
		UserDTO userDTO = null;
		if (user != null) {
			userDTO = new UserDTO(user.getId(), user.getUsername(), user.getEmail());
		}
		return userDTO;
	}
}
