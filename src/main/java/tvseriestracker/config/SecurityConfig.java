package tvseriestracker.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.HashSet;

import tvseriestracker.domain.User;
import tvseriestracker.dto.UserDTO;
import tvseriestracker.dto.converter.UserToUserDTOConverter;
import tvseriestracker.repository.UserRepository;
import tvseriestracker.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserToUserDTOConverter userConverter;

	public static final class SpringUserDetails implements UserDetails {

		private final UserDTO userDTO;
		private final String password;

		public SpringUserDetails(
			final UserDTO userDTO,
			final String password
		) {
			this.userDTO = userDTO;
			this.password = password;
		}

		public UserDTO getUserDTO() {
			return this.userDTO;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return new HashSet<>();
		}

		@Override
		public String getPassword() {
			return this.password;
		}

		@Override
		public String getUsername() {
			return this.userDTO.getUsername();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}
	}

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(buildDaoAuthenticationProvider());
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/").permitAll()
				.antMatchers("/register").permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
			.logout()
				.permitAll();
	}

	private DaoAuthenticationProvider buildDaoAuthenticationProvider(){
		final DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setUserDetailsService(username -> {
			final User user = userRepository.findByUsernameAllIgnoreCase(username);
			final UserDTO userDTO = userConverter.convert(user);

			if (user == null || userDTO == null)
				throw new UsernameNotFoundException("Username " + username + "was not found");
			else return new SpringUserDetails(userDTO, user.getPasswordHash());
		});
		daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
		return daoAuthenticationProvider;
	}
}
