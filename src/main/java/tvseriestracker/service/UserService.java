package tvseriestracker.service;

import tvseriestracker.controller.helper.RegistrationInfo;
import tvseriestracker.dto.UserDTO;

public interface UserService {
	public enum RegistrationStatus {
		SUCCESS, EXISTS, FAIL
	}

	RegistrationStatus createUser(UserDTO user, String password);
	RegistrationStatus createUser(RegistrationInfo registrationInfo);
	UserDTO findUserByUsername(String username);
}
