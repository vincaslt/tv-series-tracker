package tvseriestracker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

import tvseriestracker.domain.Show;
import tvseriestracker.domain.User;
import tvseriestracker.dto.ShowDTO;
import tvseriestracker.dto.UserDTO;
import tvseriestracker.dto.converter.ShowToShowDTOConverter;
import tvseriestracker.repository.ShowRepository;
import tvseriestracker.repository.UserRepository;
import tvseriestracker.service.ShowService;

import static java.util.stream.Collectors.toList;

@Service
public class ShowServiceImpl implements ShowService {

	ShowRepository showRepository;
	UserRepository userRepository;
	ShowToShowDTOConverter showConverter;

	@Autowired
	public ShowServiceImpl(ShowRepository showRepository, UserRepository userRepository,
						   ShowToShowDTOConverter showConverter) {
		this.showRepository = showRepository;
		this.userRepository = userRepository;
		this.showConverter = showConverter;
	}

	@Override
	@Transactional
	public boolean addShow(ShowDTO showDTO, UserDTO userDTO) {
		if (showDTO != null) {
			User user = userRepository.findOne(userDTO.getId());
			Show show = showRepository.findByImdbId(showDTO.getImdbId());
			if (show == null)
				show = showRepository.save(new Show(showDTO.getId(), showDTO.getImdbId(), showDTO.getTitle(),
						showDTO.getPosterURL(), showDTO.getRating(), showDTO.getDescription(), showDTO.getYear()));
			return show != null ? user.getShows().add(show) : false;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean removeShow(ShowDTO showDTO, UserDTO userDTO) {
		if (showDTO != null) {
			User user = userRepository.findOne(userDTO.getId());
			Show show = showRepository.findByImdbId(showDTO.getImdbId());
			if (show != null)
				return user.getShows().remove(show);
		}
		return false;
	}

	@Override
	public List<ShowDTO> findUserShows(UserDTO userDTO) {
		final User user = userRepository.findOne(userDTO.getId());
		return user.getShows().stream().map(showConverter::convert).collect(toList());
	}

	@Override
	public ShowDTO findByImdbId(String imdbId) {
		return showConverter.convert(showRepository.findByImdbId(imdbId));
	}
}
