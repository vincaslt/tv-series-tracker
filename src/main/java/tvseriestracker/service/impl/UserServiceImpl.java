package tvseriestracker.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.UUID;

import javax.transaction.Transactional;

import tvseriestracker.controller.helper.RegistrationInfo;
import tvseriestracker.domain.User;
import tvseriestracker.dto.UserDTO;
import tvseriestracker.dto.converter.UserToUserDTOConverter;
import tvseriestracker.repository.UserRepository;
import tvseriestracker.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private UserToUserDTOConverter userConverter;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserToUserDTOConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}

	@Override
	@Transactional
	public RegistrationStatus createUser(UserDTO userDTO, String password) {
		if (userDTO != null) {
			User user = new User(userDTO.getId(), userDTO.getUsername(),
					userDTO.getEmail(), encryptPassword(password));
			if (findUserByUsername(userDTO.getUsername()) != null)
				return RegistrationStatus.EXISTS;
			else if (userRepository.save(user) != null)
				return RegistrationStatus.SUCCESS;
		}
		return RegistrationStatus.FAIL;
	}

	@Override
	@Transactional
	public RegistrationStatus createUser(RegistrationInfo registrationInfo) {
		return createUser(new UserDTO(UUID.randomUUID(), registrationInfo.getUsername(),
				registrationInfo.getEmail()), registrationInfo.getPassword());
	}

	@Override
	@Transactional
	public UserDTO findUserByUsername(String username) {
		return userConverter.convert(
			userRepository.findByUsernameAllIgnoreCase(username));
	}

	public String encryptPassword(final String rawPassword) {
		final String salt = BCrypt.gensalt(10, new SecureRandom());
		return BCrypt.hashpw(rawPassword, salt);
	}
}
