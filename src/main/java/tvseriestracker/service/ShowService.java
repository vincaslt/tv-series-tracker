package tvseriestracker.service;

import java.util.List;

import tvseriestracker.dto.ShowDTO;
import tvseriestracker.dto.UserDTO;

public interface ShowService {
	boolean addShow(ShowDTO showDTO, UserDTO userDTO);
	boolean removeShow(ShowDTO showDTO, UserDTO userDTO);
	List<ShowDTO> findUserShows(UserDTO userDTO);
	public ShowDTO findByImdbId(String imdbId);
}
