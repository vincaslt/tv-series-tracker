package tvseriestracker.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "shows")
public class Show {

	@Id
	@Column(name = "id", columnDefinition = "BINARY(16)", nullable = false)
	private UUID id;

	@Column(name = "imdb_id", length = 9, nullable = false)
	private String imdbId;

	@Column(name = "title", length = 100, nullable = false)
	private String title;

	@Column(name = "poster_url", length = 255, nullable = true)
	private String posterURL;

	@Column(name = "rating", length = 3, nullable = true)
	private String rating;

	@Column(name = "description", length = 255, nullable = true)
	private String description;

	@Column(name = "year", length = 100, nullable = true)
	private String year;

	@ManyToMany
	@JoinTable(name = "users_shows",
			joinColumns = @JoinColumn(name = "show_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
	private Set<User> usersTracking = new HashSet<>();

	public Show() {
	}

	public Show(UUID id, String imdbId, String title, String posterURL, String rating, String description, String year) {
		this.id = id;
		this.imdbId = imdbId;
		this.title = title;
		this.posterURL = posterURL;
		this.rating = rating;
		this.description = description;
		this.year = year;
	}

	public UUID getId() {

		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPosterURL() {
		return posterURL;
	}

	public void setPosterURL(String posterURL) {
		this.posterURL = posterURL;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<User> getUsersTracking() {
		return usersTracking;
	}

	public void setUsersTracking(Set<User> usersTracking) {
		this.usersTracking = usersTracking;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public boolean equals(Object obj) {
		return getImdbId().equals(((Show)obj).getImdbId());
	}
}
